# README #

This README contains information for downloading the Cardiff Earth and Ocean Sciences LaTeX PhD Thesis template.

## How do I get just the template files? ##

To get the template files only, go to the Downloads option on the menu to the left and download!

## New to LaTeX? ##

There is How-To document in the how-to folder that contains a PDF document with a basic introduction to go with the template.  The template itself has examples of the major elements of the thesis document.

## How can I contribute to the template ##

If you are adapting the template, feel free to update the template with new options, particularly for formatting.  

## Who do I talk to? ##

* We're still in the process of getting everything set up.  In the mean time contact either Jamie Wilson (wilsonjd@cardiff.ac.uk) or Matthew Price (PriceMG@cardiff.ac.uk)